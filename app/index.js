var generators = require('yeoman-generator');

var composer = false; // For Slim


module.exports = generators.Base.extend({

  constructor: function() {
    // Call super constructor
    generators.Base.apply(this, arguments);
  },

  prompting: function() {
    var done = this.async();
    // This sets up a question to ask the User in order to figure out
    // what template/process to create
    this.prompt({
      choices: [ 'Email', 'HTML Site', 'PHP site'],
      type: 'list',
      name: 'template',
      message: 'Greetings Dev.\nWhat do you need?',
      default: 'Email'
    }, function (answers) {

      // Switch statement to set template path
      switch (answers.template) {
        case 'Email':
          path = 'email';
          break;
        case 'HTML Site':
          path = 'html_site';
          break;
        case 'PHP site': // Right now, assumes Slim, but this to change
          // composer = true;
          path = 'php_site';
          break;
        default:
          break;
      }
      done();
    }.bind(this));
  },

  install: function() {
    this.log('Installing dependencies');
    this.installDependencies({
      bower: false,
      npm: true,
      callback: function() {
        console.log('Dependencies have been installed!');
      }
    });
    // if (composer) {
    //   this.log('Installing Slim');
    //   this.spawnCommand('composer', ['install']);
    // }
  },

  paths: function() {
    this.destinationRoot('./build');
  },

  // Copy whatever's in the targeted template path to build/
  writing: {
    fileStructure: function() {
      this.fs.copy(
        this.templatePath(path),
        this.destinationPath()
      );

      // Copy dotfiles
      this.fs.copy(
        this.templatePath(path + '/.*'),
        this.destinationPath()
      );
    }
  }

});
