/*!
 * gulp
 * npm install 
 *
 * Console logs are used in place of the gulp-notify plugin.
 * This is because gulp-notify will throw errors if using a version of
 * Growl before 1.3 (our machines are configured with V1.2)
 */

// Load plugins
var gulp = require('gulp'),
    autoprefixer = require('gulp-autoprefixer'),
    bootlint = require('gulp-bootlint'),
    browserSync = require('browser-sync').create(),
    cache = require('gulp-cache'),
    concat = require('gulp-concat'),
    // connect = require('gulp-connect'),
    cssnano = require('gulp-cssnano'),
    del = require('del'),
    imagemin = require('gulp-imagemin'),
    jshint = require('gulp-jshint'),
    plumber = require('gulp-plumber'),
    rename = require('gulp-rename'),
    sass = require('gulp-ruby-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    uglify = require('gulp-uglify'),
    watch = require('gulp-watch'),
    w3cjs = require('gulp-w3cjs');


// Clean
gulp.task('clean', function() {
  return del(['dist/assets/styles', 'dist/assets/js', 'dist/assets/images']);
});


// Default task
gulp.task('default', ['clean'], function() {
  gulp.start('markup', 'styles', 'scripts-custom', 'scripts-vendor', 'images', 'watch');
});


// Images
gulp.task('images', function() {
  return gulp.src('src/assets/images/**/*')
    .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
    .pipe(gulp.dest('dist/assets/images'))
    .pipe(browserSync.stream());
});


// Scripts 
gulp.task('scripts-custom', function() {
  return gulp.src(['src/assets/js/**/*.js', '!src/assets/js/{vendor,vendor/**}'])
    .pipe(plumber())
    .pipe(jshint('.jshintrc'))
    .pipe(jshint.reporter('default'))
    .pipe(concat('main.js'))
    .pipe(rename({ suffix: '.min' }))
    .pipe(uglify())
    .pipe(gulp.dest('dist/assets/js'))
    .pipe(browserSync.stream());
});


gulp.task('scripts-vendor', function() {
return gulp.src('src/assets/js/vendor/**/*.js')
  .pipe(gulp.dest('dist/assets/js/vendor'));
});


// Styles
// Does not concat files (to maintain mobile/desktop separation)
gulp.task('styles', function() {
  return sass('src/assets/styles/*.scss', { sourcemap: true })
    .pipe(plumber())
    .pipe(concat('main.css'))
    .pipe(autoprefixer('last 2 version'))
    .pipe(rename({ suffix: '.min' }))
    .pipe(sourcemaps.init())
    .pipe(cssnano())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('dist/assets/styles'))
    .pipe(browserSync.stream({match: '**/*.css'}));
});

// Validate code
gulp.task('markup', function() {
  gulp.src('src/**/*.html')
    .pipe(w3cjs()) 			// Check if valid html
    .pipe(bootlint())		// Check if valid bootstrap
    .pipe(gulp.dest('dist/'))
    .pipe(browserSync.stream());
});

// Watch
gulp.task('watch', ['webserver'], function() {

  // Watch .html files
  watch('src/**/*.html', function() {
    gulp.start('markup');
  });

  // Watch .scss files
  watch('src/stylesheets/*.scss', function() {
    gulp.start('styles');
  });

  // Watch .js files
  watch(['src/assets/js/**/*.js', '!src/assets/js/{vendor,vendor/**}'], function() {
    gulp.start('scripts-custom');
  });

  watch('src/assets/js/{vendor,vendor/**}', function() {
    gulp.start('scripts-vendor');
  });

  // Watch image files
  watch('src/assets/images/**/*', function() {
    gulp.start('images');
  });
});

// Webserver
gulp.task('webserver', function() {
  browserSync.init({
    server: "./dist",
    reloadDelay: "500",
    reloadDebounce: "1000"
  });
});
