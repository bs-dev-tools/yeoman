/*
 * Gulp
 * npm install 
 *
 */

// Load plugins
var gulp = require('gulp'),
    autoprefixer = require('gulp-autoprefixer'),
    browserSync = require('browser-sync').create(),
    cache = require('gulp-cache'),
    concat = require('gulp-concat'),
    connect = require('gulp-connect-php'),
    cssnano = require('gulp-cssnano'),
    del = require('del'),
    imagemin = require('gulp-imagemin'),
    jshint = require('gulp-jshint'),
    plumber = require('gulp-plumber'),
    rename = require('gulp-rename'),
    sass = require('gulp-ruby-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    uglify = require('gulp-uglify'),
    watch = require('gulp-watch');


// Clean
gulp.task('clean', function() {
  return del(['dist/']);
});


// Default task
gulp.task('default', ['clean'], function() {

  // Copy htaccess file
  gulp.src('src/.htaccess')
    .pipe(gulp.dest('dist/'));

  gulp.start('php', 'styles', 'scripts-vendor', 'scripts-custom', 'images', 'webserver', 'watch');
});


// Images
gulp.task('images', function() {
  return gulp.src('src/assets/images/**/*')
    .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
    .pipe(gulp.dest('dist/assets/images'))
    .pipe(browserSync.stream());
});


// PHP
gulp.task('php', function() {
  gulp.src('src/**/*.php')
    .pipe(watch('src/**/*.php'))
    .pipe(gulp.dest('dist/'))
    .pipe(browserSync.stream());
});


// Scripts 
gulp.task('scripts-custom', function() {
  return gulp.src('src/assets/js/*.js')
    .pipe(plumber())
    .pipe(jshint('.jshintrc'))
    .pipe(jshint.reporter('default'))
    .pipe(concat('main.js'))
    .pipe(rename({ suffix: '.min' }))
    .pipe(uglify())
    .pipe(gulp.dest('dist/assets/js'))
    .pipe(browserSync.stream());
});


gulp.task('scripts-vendor', function() {
return gulp.src('src/assets/js/vendor/**/*.js')
  .pipe(gulp.dest('dist/assets/js/vendor'));
});


// Styles
/*  If you want files to be processed in a certain way, 
    make sure they are alphabetically sorted.
    Gulp will process files alphabetically. */
gulp.task('styles', function() {
  return sass('src/assets/styles/*.scss', { sourcemap: true })
    .pipe(plumber())
    .pipe(concat('main.css'))
    .pipe(autoprefixer('last 2 version'))
    .pipe(rename({ suffix: '.min' }))
    .pipe(sourcemaps.init())
    .pipe(cssnano())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('dist/assets/styles'))
    .pipe(browserSync.stream({match: '**/*.css'}));
});


// Watch
gulp.task('watch', function() {

  // Scripts
  watch(['src/assets/js/**/*.js', '!src/assets/js/{vendor,vendor/**}'], function() {
    gulp.start('scripts-custom');
  });

  watch('src/assets/js/vendor/*.js', function() {
    gulp.start('scripts-vendor');
  });

  // SCSS files
  watch('src/assets/styles/**/*.scss', function() {
    gulp.start('styles');
  });

  // PHP files
  watch('src/**/*.php', function() {
    gulp.start('php');
  });

  // Images
  watch('src/assets/images/**/*', function() {
    gulp.start('images');
  });
});


// Webserver
gulp.task('webserver', function() {
  connect.server({ base: 'dist' }, function () {
    browserSync.init({
      proxy: "localhost:8000",
      reloadDelay: "500",
      reloadDebounce: "1000"
    });
  });
});
