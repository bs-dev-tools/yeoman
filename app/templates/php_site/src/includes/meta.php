<head>
 <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <?php if (isset($title)) { echo "<title>". $title . "</title>";}?>
  <?php if (isset($description)) { echo "<meta name='description' content='$description'>";}?>
  <?php if (isset($keywords)) { echo "<meta name='keywords' content='$keywords'>";}?> 

    <link rel="apple-touch-icon" href="_assets/icons/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="_assets/icons/favicon.ico">

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css"> 
	<link href='https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300' rel='stylesheet' type='text/css'>
	
    <link rel="stylesheet" href="_assets/stylesheets/main.min.css">

    <script src="_assets/javascript/modernizr-2.8.3.min.js"></script>

</head>
