<div id="form" class="container">
	<div class="row">
		<div class="col-xs-12">
			<h1>Form Page</h1>
			<p>Yeah, it's not pretty on the eyes, but it's all about the functionality here.</p>
		</div>
	</div>

	<div class="row demo">
		<div class="col-xs-12">
			<form class="form-horizontal" id="demoForm" action="/form-success" method="post">
				<fieldset>

					<!-- Textual inputs -->
					<div class="row">
						<div class="col-xs-12 col-sm-8">
							<h2>Textual inputs (type = text & email)</h2>
							<p>Multi-column forms are possible, but require a lot more markup. Check out the source code to see.</p>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-4">
							<div class="form-group">
								<div class="col-xs-12">
									<label class="control-label" for="firstname">First Name</label>
									<input class="form-control" type="text" name="firstname" placeholder="First Name*" required>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-4">
							<div class="form-group">
								<div class="col-xs-12">
									<label class="control-label" for="lastname">Last Name</label>
									<input class="form-control" type="text" name="lastname" placeholder="Last Name*" required>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-4">
							<div class="form-group">
								<div class="col-xs-12">
									<label class="control-label" for="email">Email</label>
									<input class="form-control" type="email" name="email" placeholder="Email*" required>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-4">
							<div class="form-group">
								<div class="col-xs-12">
									<label class="control-label" for="input_4">Address</label>
									<input class="form-control" id="autocomplete" type="text" name="address" placeholder="Address*" required>
									<input type="hidden" id="street_number" name="street_number" />
									<input type="hidden"  id="route" name="route" />
									<input type="hidden"  id="locality" name="city"  />
									<input type="hidden"  id="administrative_area_level_1" name="state"  />
									<input type="hidden"  id="country" name="country"/>
									<input type="hidden"  id="postal_code" name="postal_code"/>
								</div>
							</div>
						</div>
					</div>
					<!-- End of multi column form -->

					<div class="row">
						<div class="col-xs-12 col-sm-8">
							<h3>Single Column forms</h3>
							<p>Single Column layouts aren't much different than the mutli columns, but are easier manage in mobile view.</p>
						</div>
					</div>
					<div class="row single-col">
						<div class="col-xs-12 col-sm-4">
							<div class="form-group">
								<div class="col-xs-12">
									<label class="control-label" for="social_security">Social Security:</label>
									<input class="form-control" type="text" name="social_security" required>
								</div>
							</div>
							<div class="form-group">
								<div class="col-xs-12">
									<label class="control-label" for="credit_card">Favorite Credit Card Number:</label>
									<input class="form-control" type="text" name="credit_card" required>
								</div>
							</div>
							<div class="form-group">
								<div class="col-xs-12">
									<label class="control-label" for="card_name">Cardholder Name:</label>
									<input class="form-control" type="text" name="card_name" required>
								</div>
							</div>
							<div class="form-group">
								<div class="col-xs-12">
									<label class="control-label" for="pin">PIN:</label>
									<input class="form-control" type="text" name="pin" required>
								</div>
							</div>
						</div>
					</div>


					<!-- End of Text inputs -->

					<!-- Radio buttons -->
					<div class="form-group">
						<div class="col-xs-12">
							<h2>Radio buttons</h2>
							<p>Of all shapes and sizes!</p>
						</div>
						<!-- Classic Radio buttons -->
						<div class="col-xs-12">
							<h3>Regular radio buttons</h3>
							<p>Resize your window to see the difference between desktop views and mobile views!</p>
							<label id="radios1-error" class="error" for="radios1">This field is required.</label>
						</div>
						<div class="col-xs-12 col-sm-2">
							<div class="radio">
								<label>
									<input id="radioBtn1" type="radio" name="radios1" value="1" required>
									Option 1
								</label>
							</div>
						</div>
						<div class="col-xs-12 col-sm-2">
							<div class="radio">
								<label>
									<input id="radioBtn2" type="radio" name="radios1" value="2">
									Option 2
								</label>
							</div>
						</div>
						<div class="col-xs-12 col-sm-2">
							<div class="radio">
								<label>
									<input id="radioBtn3" type="radio" name="radios1" value="3">
									Option 3
								</label>
							</div>
						</div>
						<div class="col-xs-12 col-sm-2">
							<div class="radio">
								<label>
									<input id="radioBtn4" type="radio" name="radios1" value="4">
									Option 4
								</label>
							</div>
						</div>
					</div>
					<!-- End Classic Radio buttons -->

					<!-- Pill-like buttons -->
					<div class="form-group">
						<div class="col-xs-12">
							<h3>Pill-like radio buttons</h3>
							<label id="radiosPill-error" class="error" for="radiosPill">This field is required.</label>
						</div>

						<div class="col-xs-12">
							<div class="btn-group" data-toggle="buttons">
								<label class="btn btn-primary">
									<input id="radioPill1" type="radio" name="radiosPill" autocomplete="off" value="1" required>
									Option 1
								</label>
								<label class="btn btn-primary">
									<input id="radioPill2" type="radio" name="radiosPill" autocomplete="off" value="2">
									Option 2
								</label>
								<label class="btn btn-primary">
									<input id="radioPill3" type="radio" name="radiosPill" autocomplete="off" value="3">
									Option 3
								</label>
								<label class="btn btn-primary">
									<input id="radioPill4" type="radio" name="radiosPill" autocomplete="off" value="4">
									Option 4
								</label>
							</div>
						</div>
					</div>
					<!-- End of Radio buttons -->

					<!-- Checkboxes -->
					<div class="row">
						<div class="col-xs-12 col-sm-8">
							<h2>Checkboxes</h2>
							<p>Validation for checkboxes must be handled differently since The Bloc usually does not group checkboxes by name. I usually run a check that at least one is checked in the submitHandler method. If none are checked, then I just return out of the function, but that's just me.</p>
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12 col-sm-2">
							<div class="checkbox">
								<label>
									<input type="checkbox" value="1">
									Check 1
								</label>
							</div>
							<div class="checkbox">
								<label>
									<input type="checkbox" value="2">
									Check 2
								</label>
							</div>
							<div class="checkbox">
								<label>
									<input type="checkbox" value="3">
									Check 3
								</label>
							</div>
						</div>
						<div class="col-xs-12 col-sm-2">
							<div class="checkbox">
								<label>
									<input type="checkbox" value="4">
									Check 4
								</label>
							</div>
							<div class="checkbox">
								<label>
									<input type="checkbox" value="5">
									Check 5
								</label>
							</div>
							<div class="checkbox">
								<label>
									<input type="checkbox" value="6">
									Check 6
								</label>
							</div>
						</div>
					</div>
					<!-- End Checkboxes -->

					<!-- Accept terms -->
					<div class="form-group">
						<div class="col-xs-12 col-sm-8">
							<h2>Accept terms</h2>
							<p>This is where the paragraph explaining how the user accepts the terms and conditions will/can be. <!--lorem-->Soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt.<!--/lorem--></p>
						</div>
						<div class="col-xs-12">
							<label id="accept-error" class="error" for="accept">This field is required.</label>
							<div class="checkbox">
								<label>
									<input type="checkbox" name="accept" value="1" required>
									I Accept
								</label>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12 col-sm-8">
							<p>Thank you for taking the time to check out this form. I hope it gives some insight as how these can be built so we can work in a more organized, streamlined, and easier fashion! Dev out yo.</p>
						</div>
					</div>
					<!-- End Accept terms -->

					<div class="form-group">
						<div class="col-xs-12">
							<input class="pull-right" type="submit" value="Submit">
						</div>
					</div>
					
				</fieldset>
			</form>
		</div>
	</div>
</div>
