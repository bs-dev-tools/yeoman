<div id="form-success" class="container">
	<div class="row">
		<div class="col-xs-12">
			<h1>Thank you! Your form has successfully submitted.</h1>
		</div>
	</div>
	<div class="row">
		<?php
		foreach ($_POST as $key => $value) {
			echo '<div class="col-xs-4">' . $key . '</div>';
			echo '<div class="col-xs-8">' . $value . '</div>';
		}
		?>
	</div>
</div>
