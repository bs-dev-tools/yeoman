<?php 
$current =  Slim\Slim::getInstance()->router()->getCurrentRoute()->getName();

 ?>

<div id="home" class="content">
	<h2><?php echo $current; ?></h2>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12">
		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi scelerisque cursus justo sodales tempus. Morbi convallis semper gravida. Morbi posuere elit eu risus facilisis, vitae ultricies diam volutpat. Curabitur commodo, diam nec imperdiet placerat, sapien nibh efficitur dolor, quis dignissim purus mi et felis. Proin ultricies varius neque, sit amet condimentum leo euismod scelerisque. Phasellus eu dolor quis nisl mollis finibus. Donec tempor a lacus id blandit. Suspendisse vitae libero tincidunt, feugiat justo sit amet, iaculis tellus. Proin facilisis porttitor magna, eu mattis libero laoreet ut. Suspendisse sed pellentesque ligula, at mollis justo. Suspendisse eu lorem elementum, sagittis tortor et, ultricies metus. 
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<h3>Examples of exit ramps</h3>
		</div>
		<div class="col-xs-12">
			<ul>
				<li>
					<a href="views/modal.php?type=exit&href=https://google.com" data-toggle="modal" data-target="#myModal">To Google</a>
				</li>
				<li>
					<a href="views/modal.php?type=exit&href=https://twitter.com" data-toggle="modal" data-target="#myModal">To Twitter</a>
				</li>
				<li>
					<a href="views/modal.php?type=exit&href=https://facebook.com" data-toggle="modal" data-target="#myModal">To Ashley Madison</a>
				</li>
			</ul>
		</div>
	</div>
</div>
