<?php
echo '
<div class="modal-header">
	<button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<h4 class="modal-title">Are you sure you want to leave this site?</h4>
</div>
<div class="modal-body">
	<p>By clicking on this link, you will be leaving this website and going to a third-party independent site. _Company_ provides these links for your information only and takes no responsibility for the content on any such website.</p>
</div>
<div class="modal-footer">
	<button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
	<a class="btn btn-primary" href="' . $external_url . '">Continue</a>
</div>
';
?>
